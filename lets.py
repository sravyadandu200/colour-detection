import cv2
import pandas as pd
img=cv2.imread('colorpic.jpg')
index=['name','color','hex','r','g','b']
csv=pd.read_csv('colors.csv',names=index,header=None)

#print(data)
def getcolor(R,G,B):
    minimum = 10000
    for i in range(len(csv)):
        d = abs(R- int(csv.loc[i,"r"])) + abs(G- int(csv.loc[i,"g"]))+ abs(B- int(csv.loc[i,"b"]))
        if(d<=minimum):
            minimum = d
            cname = csv.loc[i,"color"]
    return cname

def click_event(event,x,y,flags,param):
        
        if event==cv2.EVENT_LBUTTONDOWN:
            img=cv2.imread('colorpic.jpg')
            blue=img[y,x,0]
            green=img[y,x,1]
            red=img[y,x,2]
            font=cv2.FONT_HERSHEY_SIMPLEX
            l=getcolor(red,green,blue)
            
            stbgr=l+" "+"blue:"+str(blue)+" green: "+str(green)+" red: "+str(red)
            cv2.rectangle(img,(20,20), (510,60), (int(blue),int(green),int(red)), -1)
            if (int(red)+int(blue)+int(green))>=600:
                cv2.putText(img,stbgr,(50,40),font,.5,(0,0,0),2)
            else:
                cv2.putText(img,stbgr,(50,40),font,.5,(255,255,255),2)

            cv2.imshow('image',img)


cv2.imshow('image',img)
cv2.setMouseCallback('image',click_event)
ls=cv2.waitKey(0)

cv2.destroyAllWindows()



